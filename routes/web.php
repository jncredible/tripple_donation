<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/profile/bankinfo','Profile@bank_info');

Route::get('/profile/info','Profile@profile_info');

Route::get('/profile/bitcoinaddressinfo','Profile@bitcoin_address_info');

Route::get('/dashboard/main','Dashboard@dashboard_info');

Route::get('/donation/tree','Donation@donation_tree');