<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Profile extends Controller
{
    //

    public function bank_info (){
    	return view ('profiles.bank_information');
    }

    public function profile_info (){
    	return view ('profiles.profile');
    }

    public function bitcoin_address_info (){
    	return view ('profiles.bitcoin_address');
    }
}
