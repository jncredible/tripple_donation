@extends('layouts.app')

@section('styles')

<style>
      .card-design{
        box-shadow: 5px 5px 10px .5px #a5a5a5 !important;
        background: rgba(255,255,255,0.8) !important;
        top: 180%;
        transform: translateY(-50%);
      }
</style>

@endsection

@section('content')
   
     
          <div class="row" style="height: 100%;">
        
            <div class="col-md-4"></div>
            <div class="col-md-4 grid-margin">
              <div class="card card-design">
                <div class="card-body align-content-center">
                  <h4 class="card-title font-weight-bold">Bank Details</h4>
                  <form class="forms-sample">
                    <div class="form-group">
                      <label for="exampleInputName1">Bitcoin Address</label>
                      <input type="text" class="form-control" id="exampleInputName1" placeholder="Bitcoin Address">
                    </div>
                    <button type="submit" class="btn btn-success mr-2">Save</button>
                    <button class="btn btn-light">Cancel</button>
                  </form>
                </div>
            </div>
        </div>
     </div>
        <!-- content-wrapper ends -->
       

@endsection