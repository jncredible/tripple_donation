@extends('layouts.app')

@section('styles')

<style>
      .card-design{
        box-shadow: 5px 5px 10px .5px #a5a5a5 !important;
        background: rgba(255,255,255,0.8) !important;
        top: 70%;
        transform: translateY(-50%);
      }
</style>

@endsection

@section('content')
   
     
          <div class="row" style="height: 100%;">
        
            <div class="col-md-3"></div>
            <div class="col-md-6 grid-margin">
              <div class="card card-design">
                <div class="card-body align-content-center">
                  <h4 class="card-title font-weight-bold">GENERAL INFORMATION</h4>
                 <!--  <p class="card-description">
                    Basic form elements
                  </p> -->
                  <form class="forms-sample">

                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label"  for="exampleInputName1">First Name</label>
                          <div  class="col-sm-8">
                          <input type="text" class="form-control" id="exampleInputName1" placeholder="Firstname">
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label"  for="exampleInputName1">Last Name</label>
                          <div  class="col-sm-8">
                          <input type="text" class="form-control" id="exampleInputName1" placeholder="Lastname">
                          </div>
                        </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label"  for="exampleInputName1">Gender</label>
                          <div  class="col-sm-8">
                          <select class="form-control">
                              <option>Male</option>
                              <option>Female</option>
                            </select>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label"  for="exampleInputName1">Marital Status</label>
                          <div  class="col-sm-8">
                          <select class="form-control">
                              <option>Single</option>
                              <option>Married</option>
                              <option>Widowed</option>
                              <option>Separated</option>
                            </select>
                          </div>
                        </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label"  for="exampleInputName1">Email Address</label>
                          <div  class="col-sm-8">
                          <input type="email" class="form-control" id="exampleInputEmail3" placeholder="Email Address">
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label"  for="exampleInputName1">Contact Number</label>
                          <div  class="col-sm-8">
                          <input type="text" class="form-control" id="exampleInputName1" placeholder="+639">
                          </div>
                        </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label"  for="exampleInputName1">Address</label>
                          <div  class="col-sm-8">
                          <input type="text" class="form-control" id="exampleInputName1" placeholder="Complete Address">
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label"  for="exampleInputName1">City</label>
                          <div  class="col-sm-8">
                          <input type="text" class="form-control" id="exampleInputName1" placeholder="City">
                          </div>
                        </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label"  for="exampleInputName1">State/Province</label>
                          <div  class="col-sm-8">
                          <input type="text" class="form-control" id="exampleInputName1" placeholder="State/Province">
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label"  for="exampleInputName1">Country</label>
                          <div  class="col-sm-8">
                          <input type="text" class="form-control" id="exampleInputName1" placeholder="Country">
                          </div>
                        </div>
                      </div>
                  </div>

                    <div class="form-group">
                      <label>Profile Photo</label>
                      <input type="file" name="img[]" class="file-upload-default">
                      <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image">
                        <span class="input-group-append">
                          <button class="file-upload-browse btn btn-info" type="button">Upload</button>
                        </span>
                      </div>
                    </div>

                  
                    <div class="form-group">
                      <label for="exampleTextarea1">Bio</label>
                      <textarea class="form-control" id="exampleTextarea1" rows="2"></textarea>
                    </div>
                    <button type="submit" class="btn btn-success mr-2">Save</button>
                    <button class="btn btn-light">Cancel</button>
                  </form>
                </div>
            </div>
        </div>
     </div>

        <!-- content-wrapper ends -->
       

@endsection