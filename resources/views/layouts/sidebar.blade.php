<nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <div class="nav-link">
              <div class="user-wrapper">
                <div class="profile-image">
                  <img src="../../images/faces/face1.jpg" alt="profile image">
                </div>
                <div class="text-wrapper">
                  <p class="profile-name">Richard V.Welsh</p>
                  <div>
                    <small class="designation text-muted">Manager</small>
                    <span class="status-indicator online"></span>
                  </div>
                </div>
              </div>
              <button class="btn btn-success btn-block">New Project
                <i class="mdi mdi-plus"></i>
              </button>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{URL::to('/dashboard/main')}}">
              <i class="menu-icon mdi mdi-television"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-account"></i>
              <span class="menu-title">PROFILE SECTION</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="{{URL::to('/profile/info')}}">
                    <i class="menu-icon mdi mdi-account-circle"></i>
                    <span class="menu-title">Profile</span>
                  </a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="{{URL::to('/profile/bankinfo')}}">
                    <i class="menu-icon mdi mdi-bank"></i>
                    <span class="menu-title">Bank Information</span>
                  </a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="{{URL::to('/profile/bitcoinaddressinfo')}}">
                    <i class="menu-icon mdi mdi-wallet-travel"></i>
                    <span class="menu-title">Wallet</span>
                  </a>
                </li>

              </ul>
            </div>
          </li>

          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-donation" aria-expanded="false" aria-controls="ui-donation">
              <i class="menu-icon mdi mdi-plus-network"></i>
              <span class="menu-title">DOWNLINE SECTION</span>
              <i class="menu-arrow"></i>
            </a>

            <div class="collapse" id="ui-donation">
              <ul class="nav flex-column sub-menu">
              <li class="nav-item">
                <a class="nav-link" href="{{URL::to('/donation/tree')}}">
                  <i class="menu-icon mdi mdi-account-network "></i>
                  <span class="menu-title">Donation Tree</span>
                </a>
              </li>

              </ul>
            </div>
          </li>

          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
              <i class="menu-icon mdi mdi-restart"></i>
              <span class="menu-title">User Pages</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="auth">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="../../pages/samples/blank-page.html"> Blank Page </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="../../pages/samples/login.html"> Login </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="../../pages/samples/register.html"> Register </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="../../pages/samples/error-404.html"> 404 </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="../../pages/samples/error-500.html"> 500 </a>
                </li>

                @yield('link')
              </ul>
            </div>
          </li>
        </ul>
      </nav>