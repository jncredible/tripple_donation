<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableManageBvHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manage_bv_history', function (Blueprint $table) {
            $table->increments('id');
            $table->string('income_id');
            $table->string('downline_id');
            $table->integer('level');
            $table->bigInteger('bv');
            $table->string('position');
            $table->string('description');
            $table->date('date');
            $table->timestamp('ts')->nullable();
            $table->integer('status');
            $table->string('pair');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manage_bv_history');
    }
}
