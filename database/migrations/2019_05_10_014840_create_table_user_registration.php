<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserRegistration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_registration', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('ref_id');
            $table->string('nom_id');
            $table->string('username');
            $table->string('password');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->text('address');
            $table->text('city');
            $table->text('state');
            $table->text('country');
            $table->string('zipcode');
            $table->string('telephone');
            $table->integer('admin_status');
            $table->string('user_status');
            $table->string('registration_date');
            $table->string('t_code');
            $table->string('user_plan');
            $table->string('designation');
            $table->text('aboutus');
            $table->string('dob');
            $table->string('sex');
            $table->text('image');
            $table->string('acc_name');
            $table->string('ac_no');
            $table->string('bank_nm');
            $table->string('swift_code');
            $table->string('user_rank_name');
            $table->timestamp('ts')->nullable();
            $table->string('married_status');
            $table->string('last_login_date');
            $table->string('current_login_date');
            $table->string('binary_pos');
            $table->string('id_card');
            $table->string('id_no');
            $table->string('master_account');
            $table->string('issued');
            $table->string('product_type');
            $table->date('issue_date');
            $table->string('admin_remark1');
            $table->string('nom_name');
            $table->string('nom_relation');
            $table->string('nom_mobile');
            $table->string('nom_dob');
            $table->string('bank_state');
            $table->string('ben_fullname');
            $table->string('ben_nric');
            $table->string('gtb_wallet_address');
            $table->integer('ref_count');
            $table->string('remember_token');
            $table->integer('is_admin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_registration');
    }
}
