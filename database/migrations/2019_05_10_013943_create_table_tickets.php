<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('subject');
            $table->string('tasktype');
            $table->string('description');
            $table->string('detail_desc');
            $table->string('response_email');
            $table->string('priority');
            $table->integer('status');
            $table->date('t_date');
            $table->date('c_t_date');
            $table->string('response');
            $table->string('user_name');
            $table->string('ticket_no');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
