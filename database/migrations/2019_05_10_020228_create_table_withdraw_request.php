<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableWithdrawRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdraw_request', function (Blueprint $table) {
            $table->increments('id');
            $table->string('transaction_number');
            $table->string('user_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('acc_name');
            $table->string('account_number');
            $table->string('bank_nm');
            $table->string('swift_code');
            $table->string('request_amount');
            $table->text('description');
            $table->integer('status');
            $table->string('posted_date');
            $table->text('admin_remark');
            $table->string('admin_response_date');
            $table->string('withdraw_wallet');
            $table->string('total_paid_amount');
            $table->string('transaction_charge');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdraw_request');
    }
}
