<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminPrivileges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_privileges', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('privilege_id');
            $table->string('privilege_page');
            $table->date('date');
            $table->timestamp('add_date_time')->nullable();
            $table->timestamp('last_modify')->nullable();
            $table->integer('status');
            $table->integer('admin_id')->bigInt();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_privileges');
    }
}
