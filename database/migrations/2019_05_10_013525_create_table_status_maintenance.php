<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStatusMaintenance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_maintenance', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('sponsor_commission');
            $table->string('binary_bonus');
            $table->string('capping');
            $table->string('amount');
            $table->longText('description');
            $table->string('pb');
            $table->string('returns');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_maintenance');
    }
}
