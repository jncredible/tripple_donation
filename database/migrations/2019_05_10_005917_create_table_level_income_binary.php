<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLevelIncomeBinary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('level_income_binary', function (Blueprint $table) {
            $table->increments('id');
            $table->string('down_id');
            $table->string('income_id');
            $table->string('leg');
            $table->integer('level');
            $table->date('l_date');
            $table->smallInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('level_income_binary');
    }
}
