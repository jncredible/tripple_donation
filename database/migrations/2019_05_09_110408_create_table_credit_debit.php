<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCreditDebit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_debit', function (Blueprint $table) {
            $table->increments('id');
            $table->string('transaction_no');
            $table->string('user_id');
            $table->string('credit_amt');
            $table->string('admin_charge');
            $table->string('receiver_id');
            $table->string('sender_id');
            $table->string('receive_date');
            $table->string('type');
            $table->string('trandescription');
            $table->string('cause');
            $table->string('remark');
            $table->string('invoice_no');
            $table->string('product_name');
            $table->string('status');
            $table->string('ewallet_used_by');
            $table->timestamp('ts')->nullable();
            $table->string('current_url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_debit');
    }
}
