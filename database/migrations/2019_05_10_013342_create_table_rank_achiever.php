<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRankAchiever extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rank_achiever', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('last_rank');
            $table->string('move_rank');
            $table->timestamp('ts')->nullable();
            $table->string('qualify_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rank_achiever');
    }
}
