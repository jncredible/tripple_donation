<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLifejacketSubscription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lifejacket_subscription', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->double('amount');
            $table->string('pay_type');
            $table->string('remark');
            $table->timestamp('ts')->nullable();
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lifejacket_subscription');
    }
}
