<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMessageSender extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_sender', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('subject');
            $table->string('message');
            $table->string('receiver_id');
            $table->string('sender_id');
            $table->date('msg_date');
            $table->string('receiver_name');
            $table->string('sender_name');
            $table->integer('read_sender');
            $table->integer('read_receiver');
            $table->timestamp('ts')->nullable();
            $table->integer('status');
            $table->text('file_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_sender');
    }
}
