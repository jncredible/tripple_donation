<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('username');
            $table->string('password');
            $table->string('email');
            $table->string('name');
            $table->string('image');
            $table->string('type');
            $table->date('date');
            $table->timestamp('add_date_time')->nullable();
            $table->timestamp('last_modify')->nullable();
            $table->integer('status');
            $table->timestamp('last_login')->nullable();
            $table->timestamp('last_logout')->nullable();
            $table->integer('login_status');
            $table->string('website_logo');
            $table->string('transaction_pwd');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin');
    }
}
